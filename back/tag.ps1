#$credential = Get-Credential
#$credential.Password | ConvertFrom-SecureString | Set-Content ".\encrypted_password.txt"
$username = "a-smclaughlin"
$encrypted = Get-Content .\encrypted_password.txt | ConvertTo-SecureString
$credential = New-Object System.Management.Automation.PsCredential($username, $encrypted)

Connect-VIServer -Server  he1-engvc-01.headquarters.healthedge.com. -Protocol https -Credential $credential
#Connect-VIServer -Server  he1-vcs-01.headquarters.healthedge.com. -Protocol https -Credential $credential
#Connect-VIServer -Server  he2-vcs-01.headquarters.healthedge.com. -Protocol https -Credential $credential
#Connect-VIServer -Server  he1-vxvcs-01.healthedge.biz. -Protocol https -Credential $credential
#Connect-VIServer -Server  he2-vxvcs-01.healthedge.biz. -Protocol https -Credential $credential




$tag1= "One"
$FileContent = Get-content vms.txt
foreach($VM in $FileContent)
{
#Get-VM thc-ora-pr002 | Get-TagAssignment -Category "Patching Group" |remove-tagassignment -confirm:$false
Get-VM $VM | Get-TagAssignment -Category "Patching Group" |remove-tagassignment -confirm:$false
#Get-VM thc-ora-pr002 | New-TagAssignment -Tag $tag1
Get-VM $VM | New-TagAssignment -Tag $Tag1
Write-Host "Tag Assignment completed"
}

